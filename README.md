# Invisible Spaceship wp_plugins README #

Just a small collection of plugins written to learn wordpress

### Plugins ###

* is_socialwidget - v1.0 - add a fb like button to a post automatically

### How do I get set up? ###

* Each plugin will reside in it's own directory of the same name.
* Copy the contents of that directory into your wp-content/plugins/ directory
* That's it!

### Contact ###

* greg@invisiblespaceship.com