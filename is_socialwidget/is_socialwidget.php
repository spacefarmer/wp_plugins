<?php
/**
 * @package is_socialwidget
 * @version 1.0
 */

/*
Plugin Name: is_socialwidget
Plugin URI: 
Description: Adds a social button widget to each post
Author: Greg Holkenbrink (greg@invisiblespaceship.com)
Version: 1.0
Author URI: http://www.invisiblespaceship.com
*/

function insert_socialwidget( $content ) {
	$curpage = get_permalink();
	$socialwidget = "<div class=\"fb-like\" data-href=\"".$curpage."\" data-layout=\"standard\" data-action=\"like\" data-show-faces=\"true\" data-share=\"true\"></div>";
	if( is_single() ) { $content .= $socialwidget; }
	return $content;
}

add_filter( 'the_content', 'insert_socialwidget' );

function output_socialinclude() {
	$socialscript = "<div id=\"fb-root\"></div>
	<script>(function(d, s, id) {
  	var js, fjs = d.getElementsByTagName(s)[0];
  	if (d.getElementById(id)) return;
  	js = d.createElement(s); js.id = id;
  	js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4\";
  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>";

	echo $socialscript;
}

add_action( 'wp_print_scripts', 'output_socialinclude' );

?>